import Vue from 'vue';
import Buefy from 'buefy';
import { sync } from 'vuex-router-sync';
import App from './App.vue';
import titleMixin from './helpers/title';
import { createRouter } from './router';
import { createStore } from './store';
import Default from './layouts/Default.vue';
import Home from './layouts/Home.vue';

Vue.mixin(titleMixin);
Vue.mixin({
    computed: {
        user() {
            return this.$store.state.user;
        },
    },
});

Vue.component('DefaultLayout', Default);
Vue.component('HomeLayout', Home);
Vue.use(Buefy);

export function createApp() {
    // create router instance
    const router = createRouter();
    const store = createStore();

    sync(store, router);

    const app = new Vue({
        // inject router into root Vue instance
        router,
        store,
        render: (h) => h(App),
    });

    // return both the app and the router
    return { app, router, store };
}
