import Vue from 'vue';

import ProgressBar from 'Components/ProgressBar.vue';
import { createApp } from './app';

const { app, router, store } = createApp();
Vue.prototype.$bar = new Vue(ProgressBar).$mount();
const bar = Vue.prototype.$bar;
document.body.appendChild(bar.$el);

Vue.mixin({
    beforeRouteUpdate(to, from, next) {
        const { asyncData } = this.$options;
        if (asyncData) {
            asyncData({
                store: this.$store,
                route: to,
            }).then(next).catch(next);
        } else {
            next();
        }
    },
});

if (window.__INITIAL_STATE__) {
    store.replaceState(window.__INITIAL_STATE__);
}

router.onReady(() => {
    router.beforeResolve((to, from, next) => {
        const matched = router.getMatchedComponents(to);
        const prevMatched = router.getMatchedComponents(from);
        let diffed = false;
        // eslint-disable-next-line no-return-assign
        const activated = matched.filter((c, i) => diffed || (diffed = (prevMatched[i] !== c)));
        const asyncDataHooks = activated.map((c) => c.asyncData).filter((_) => _);

        if (!asyncDataHooks.length) {
            return next();
        }
        bar.start();
        Promise.all(asyncDataHooks.map((hook) => hook({ store, route: to })))
            .then(() => {
                bar.finish();
                next();
            })
            .catch(next);
    });
    router.beforeEach((to, from, next) => {
        const { user } = store.state;

        if (to.meta.auth && !user) {
            return next('/login');
        }
        return next();
    });

    app.$mount('#app');
});

// if ('https:' === location.protocol && navigator.serviceWorker) {
//     navigator.serviceWorker.register('/service-worker.js')
// }
navigator.serviceWorker.register('/service-worker.js');
