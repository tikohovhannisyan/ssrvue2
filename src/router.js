import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const Home = () => import(/* webpackChunkName: "Home" */ './views/Home.vue');
const About = () => import(/* webpackChunkName: "About" */ './views/About.vue');
const NotFound = () => import(/* webpackChunkName: "NotFound" */ './views/NotFound.vue');
const Login = () => import(/* webpackChunkName: "Login" */ './views/Login.vue');
const Profile = () => import(/* webpackChunkName: "Profile" */ './views/Profile.vue');

export function createRouter() {
    return new Router({
        mode: 'history',
        fallback: false,
        scrollBehavior: () => ({ y: 0 }),
        routes: [
            { path: '/', component: Home },
            { path: '/about', component: About, meta: { auth: false } },
            { path: '/profile', component: Profile, meta: { auth: true } },
            { path: '/login', component: Login, meta: { guest: true } },
            { path: '/404', name: '404', component: NotFound },
        ],
    });
}
