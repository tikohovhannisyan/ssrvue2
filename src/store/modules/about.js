import { fetchCatFacts } from '../../api';

export default {
    namespaced: true,
    state: () => ({
        catFacts: [],
    }),
    actions: {
        FETCH_CAT_FACTS: ({ commit }) => fetchCatFacts().then((res) => {
            commit('SET_CAT_FACTS', res.data.all);
        }),
    },
    mutations: {
        SET_CAT_FACTS: (state, data) => {
            state.catFacts = data;
        },
    },
};
