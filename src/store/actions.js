import { fetchCatFacts, me, login } from '../api';

export default {
    FETCH_CAT_FACTS: ({ commit }) => fetchCatFacts().then((res) => {
        commit('SET_CAT_FACTS', res.data.all);
    }),
    FETCH_USER: async ({ commit }, payload) => {
        const user = await me(payload);
        commit('SET_USER', user);
    },
    LOGIN: async ({ dispatch }, payload) => {
        const user = await login(payload);
        if (user) {
            document.cookie = `token=${user.token}`;
            await dispatch('FETCH_USER', user.token);
        }
    },
    LOGOUT: ({ commit }) => {
        document.cookie = 'token =; expires=expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        commit('SET_USER', null);
    },
};
