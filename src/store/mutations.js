export default {
    SET_CAT_FACTS: (state, data) => {
        state.catFacts = data;
    },
    SET_USER: (state, data) => {
        state.user = data;
    },
};
