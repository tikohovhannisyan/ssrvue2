import axios from 'axios';

export function createHTTP() {
    let http;
    if (process.__HTTP__) {
        http = process.__HTTP__;
    } else {
        // TODO separate server/client axios and create base config
        process.__HTTP__ = axios.create();
        http = process.__HTTP__;
    }
    return http;
}
