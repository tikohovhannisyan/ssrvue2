import { createHTTP } from './create-http';

const http = createHTTP();

export function fetchCatFacts() {
    return http.get('https://cat-fact.herokuapp.com/facts');
}

export async function login(data) {
    try {
        const user = await http.post('http://localhost:3000/users/login', data, {
            Accept: 'application/json',
        });
        return user.data;
    } catch {
        return null;
    }
}

export async function me(data) {
    try {
        const user = await http.get('http://localhost:3000/users/me', {
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${data}`,
            },
        });
        return user.data;
    } catch {
        return null;
    }
}
