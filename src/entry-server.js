import { createApp } from './app';

const isDev = process.env.NODE_ENV !== 'production';

export default (context) => new Promise((resolve, reject) => {
    const s = isDev && Date.now();
    const { app, router, store } = createApp();
    const { url, token } = context;
    const { fullPath } = router.resolve(url).route;

    if (fullPath !== url) {
        return reject({ url: fullPath });
    }
    router.push(url);

    router.onReady(async () => {
        const matchedComponents = router.getMatchedComponents();
        const requiredAuth = router.currentRoute.meta.auth;
        const requiredGuest = router.currentRoute.meta.guest;

        if (token) {
            await store.dispatch('FETCH_USER', token);
        }
        if (requiredAuth && !store.state.user) {
            return reject({ code: 401 });
        }

        if (requiredGuest && store.state.user) {
            return reject({ url: '/profile' });
        }

        if (!matchedComponents.length) {
            return reject({ code: 404 });
        }
        Promise.all(matchedComponents.map(({ asyncData }) => asyncData && asyncData({
            store,
            route: router.currentRoute,
        }))).then(() => {
            isDev && console.log(`data pre-fetch: ${Date.now() - s}ms`);
            context.state = store.state;
            resolve(app);
        }).catch(reject);
    }, reject);
});
