const webpack = require('webpack');
const { merge } = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const SWPrecachePlugin = require('sw-precache-webpack-plugin');
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin');
const base = require('./webpack.base.config');

const isProd = process.env.NODE_ENV === 'production';

const config = merge(base, {
    entry: {
        app: './src/entry-client.js',
    },
    resolve: {
        alias: {
            'create-api': './create-api-client.js',
        },
    },
    module: {
        rules: [
            {
                test: /\.(sc)ss$/,
                use: [
                    isProd ? MiniCssExtractPlugin.loader : 'vue-style-loader',
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            additionalData: '@import \'@/assets/theme.scss\';',
                        },
                    },
                ],
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                ],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: isProd ? '[name].[chunkhash].css' : '[name].css',
            chunkFilename: isProd ? '[name].[chunkhash].css' : '[name].css',
        }),
        new VueSSRClientPlugin(),
    ],
});

if (process.env.NODE_ENV === 'production') {
    config.plugins.push(
        // auto generate service worker
        new SWPrecachePlugin({
            cacheId: 'vue-hn',
            filename: 'service-worker.js',
            minify: true,
            dontCacheBustUrlsMatching: /./,
            staticFileGlobsIgnorePatterns: [/\.map$/, /\.json$/],
            runtimeCaching: [
                {
                    urlPattern: '/about',
                    handler: 'networkFirst',
                },
                {
                    urlPattern: '/home',
                    handler: 'networkFirst',
                },
            ],
        }),
    );
}

module.exports = config;
