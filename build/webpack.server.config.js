const webpack = require('webpack');
const { merge } = require('webpack-merge');
const VueSSRServerPlugin = require('vue-server-renderer/server-plugin');
const nodeExternals = require('webpack-node-externals');
const base = require('./webpack.base.config');

const isProd = process.env.NODE_ENV === 'production';

module.exports = merge(base, {
    target: 'node',
    entry: {
        app: './src/entry-server.js',
    },
    devtool: '#source-map',
    output: {
        filename: 'server-bundle.js',
        libraryTarget: 'commonjs2',
    },
    resolve: {
        alias: {
            'create-api': './create-api.server.js',
        },
    },

    externals: nodeExternals({
        whitelist: /\.css$/,
    }),
    module: {
        rules: [
            {
                test: /\.styl(us)?$/,
                use: [
                    'null-loader',
                ],
            },
            {
                test: /\.(c)ss$/,
                use: [
                    'null-loader',
                ],
            },
            {
                test: /\.(sc)ss$/,
                use: [
                    'null-loader',
                ],
            },
        ],
    },
    plugins: [
        new VueSSRServerPlugin(),
    ],
});
