const path = require('path');
const webpack = require('webpack');
const { VueLoaderPlugin } = require('vue-loader');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');

const resolve = (dir) => path.join(__dirname, '../', dir);
const isProd = process.env.NODE_ENV === 'production';
function setPlugin() {
    const base = [new VueLoaderPlugin()];
    const dev = [new FriendlyErrorsPlugin()];
    const prod = [];
    return base.concat(isProd ? prod : dev);
}
const base = {
    mode: isProd ? 'production' : 'development',
    devtool: isProd ? false : 'cheap-eval-source-map',
    output: {
        path: path.resolve(__dirname, '../dist'),
        publicPath: '/dist/',
        filename: '[name].[chunkhash].js',
    },
    resolve: {
        alias: {
            vue$: 'vue/dist/vue.esm.js',
            public: path.resolve(__dirname, '../public'),
            '@': path.resolve(__dirname, '../src'),
            Components: path.resolve(__dirname, '../src/components'),
        },
    },
    module: {
        noParse: /es6-promise\.js$/, // avoid webpack shimming process
        rules: [
            {
                test: /\.vue$/,
                use: [
                    {
                        loader: 'vue-loader',
                        options: {
                            extractCSS: isProd,
                        },
                    },
                    'vue-svg-inline-loader',
                ],
                include: resolve('src'),

            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: (file) => (
                    /node_modules/.test(file) && !/\.vue\.js/.test(file)
                ),
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    limit: 10000,
                    esModule: false,
                    name: '[name].[ext]?[hash]',
                },
            },
        ],
    },
    plugins: setPlugin(),
};

module.exports = base;
