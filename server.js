// const bundle = require('./dist/vue-ssr-server-bundle.json');
const fs = require('fs');
const path = require('path');
const express = require('express');
const cookieParser = require('cookie-parser');
const { createBundleRenderer } = require('vue-server-renderer');

const isProd = process.env.NODE_ENV === 'production';
const resolve = (file) => path.resolve(__dirname, file);
const serverInfo = `express/${require('express/package.json').version} vue-server-renderer/${require('vue-server-renderer/package.json').version}`;

const server = express();
server.use(cookieParser());
const serve = (_path, cache) => express.static(resolve(_path), {
    maxAge: cache && isProd ? 1000 * 60 * 60 * 24 * 30 : 0,
});

function createRenderer(bundle, options) {
    try {
        return createBundleRenderer(bundle, Object.assign(options, {
            basedir: resolve('./dist'),
            runInNewContext: true,
        }));
    } catch (err) {
        console.log(err);
        return null;
    }
}

let renderer;
let readyPromise;
const templatePath = resolve('./src/index.template.html');
if (isProd) {
    const template = fs.readFileSync(templatePath, 'utf-8');
    const bundle = require('./dist/vue-ssr-server-bundle.json');
    const clientManifest = require('./dist/vue-ssr-client-manifest.json');
    renderer = createRenderer(bundle, {
        template,
        clientManifest,
    });
} else {
    readyPromise = require('./build/setup-dev-server')(
        server,
        templatePath,
        (bundle, options) => {
            renderer = createRenderer(bundle, options);
        },
    );
}

function render(req, res) {
    const s = Date.now();
    res.setHeader('Content-Type', 'text/html');
    res.setHeader('Server', serverInfo);
    const { token } = req.cookies;
    const handleError = (err) => {
        if (err.url) {
            res.redirect(err.url);
        } else if (err.code === 404) {
            res.status(404).send('404 | Page Not Found');
        } else if (err.code === 401) {
            res.redirect('/login');
        } else {
            // Render Error Page or Redirect
            res.status(500).send('500 | Internal Server Error');
            console.error(`error during render : ${req.url}`);
            console.error(err.stack);
        }
    };

    const context = {
        title: 'YooScouts',
        meta: '',
        url: req.url,
        token,
    };
    renderer.renderToString(context, (err, html) => {
        if (err) {
            return handleError(err);
        }
        res.send(html);
        if (!isProd) {
            console.log(`whole request: ${Date.now() - s}ms`);
        }
    });
}

// TODO render funciton runs if no item in public
server.use('/dist', serve('./dist', true));
server.use('/public', serve('./public', true));
server.use('/manifest.json', serve('./manifest.json', true));
server.use('/service-worker.js', serve('./dist/service-worker.js'));

server.get('*', isProd ? render : (req, res) => {
    readyPromise.then(() => render(req, res));
});

const port = process.env.PORT || 8080;
server.listen(port, () => {
    console.log(`server started at localhost:${port}`);
});
